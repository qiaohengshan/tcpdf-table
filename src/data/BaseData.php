<?php
/**
 * User: Jason Wang
 * 基础数据
 */

namespace joyqhs\PdfTable\data;


class BaseData
{
    public $pdf;
    public $data = [];
}